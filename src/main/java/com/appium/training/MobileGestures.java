package com.appium.training;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class MobileGestures {

	MobileDriver driver;

	@Test
	public void TC_001() {

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAUtomator2");
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6.0.1");
		caps.setCapability(MobileCapabilityType.APP,
				"<provide path of APK>");
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "08a7a08127e7c5f5");
		caps.setCapability(MobileCapabilityType.FULL_RESET, false);
		caps.setCapability(MobileCapabilityType.NO_RESET, false);
		// caps.setCapability("appPackage", "com.rta.rtadubai");
		// caps.setCapability("appActivity",
		// "com.rta.rtadubai.MainDashboard.Splash");

		try {
			driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		WebElement languageTxt = driver.findElement(By.xpath("//android.widget.TextView[@text='English']"));

		WebElement doneBtn = driver.findElement(By.id("done_button"));

		doneBtn.getLocation().getX();
		if (languageTxt.isDisplayed()) {
			Assert.assertEquals(languageTxt.getText(), "English", "Verify English Text is Displayed");
			driver.tap(1, doneBtn, 500);
		}

	}

	public static void swipe(MobileDriver driver, int start, int end) {

		Dimension d = driver.manage().window().getSize();
		int starty = d.height * start;
		int endy = d.height * end;
		int startx = d.width / 2;

		driver.swipe(startx, starty, startx, endy, 1000);

	}

	public static void swipeUsingTouchAction(MobileDriver driver, int start, int end) {

		Dimension d = driver.manage().window().getSize();
		int starty = d.width * start;
		int endy = d.width * end;
		int startx = d.height / 2;
		new TouchAction(driver).press(startx, starty).waitAction(500).moveTo(startx, endy).release().perform();
		
	}
	
	
	public static void swipeUsingTouchActionWrtElement(MobileDriver driver, int start, int end) {

		Dimension d = driver.manage().window().getSize();
		int starty = d.width * start;
		int endy = d.width * end;
		int startx = d.height / 2;
		
		
		new TouchAction(driver).press(startx, starty).waitAction(500).moveTo(startx, endy).release().perform();
		
	}
	
	
	public static void multiTouch(MobileDriver driver, WebElement e1, WebElement e2){
		
		TouchAction t1 = new TouchAction(driver);
				t1.press(e1).moveTo(e1).release();
		
		TouchAction t2 = new TouchAction(driver);
				t2.press(e2).moveTo(e2).release();
		
		new MultiTouchAction(driver).add(t1).add(t2).perform();
	}

}
