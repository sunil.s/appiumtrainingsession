package com.appium.training.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.appium.training.init.TestDataCoulmns;
import com.appium.training.library.GenericLib;
import com.appium.training.util.MobileActionUtil;

import io.appium.java_client.android.AndroidDriver;

public class SplashScreen implements TestDataCoulmns {
	
	AndroidDriver driver;
	
	
	/** 
	 * @author Sunil S
	 * @descrption Constructor to initialize mobile elements
	 * @param driver
	 */
	public SplashScreen(AndroidDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	/** MOBILE ELEMENTS DELCARATION **/ 
	
	@FindBy(id="title")
	private WebElement pleaseSelectYourLanguageTxt;
	
	@FindBy(xpath="//android.widget.Text[@text='English']")
	private WebElement langaugeTxt;
	
	@FindBy(id="bottom_button")
	private WebElement doneBtn;
	
	@FindBy(id = "plate_number_select_222")
	private WebElement plateSourceDrpDwn;

	@FindBy(id = "plate_type_select_22")
	private WebElement plateTypeDrpDwn;
	
	@FindBy(id="plate_number_select_222_33")
	private WebElement plateCodeDrpDwn;
	
	@FindBy(id="myautocomplete")
	private WebElement plateNumberTxtFld;
	
	/**
	 * @author Sunil S
	 * @description Method to verify splash screen is displayed
	 * @param sFilePath
	 * @throws IOException
	 */
	public void verifySplashScren(String sFilePath) throws IOException{
		MobileActionUtil.waitForElement(pleaseSelectYourLanguageTxt, driver, "Verify Please Select Your Language Text", 15);
		String actRes = pleaseSelectYourLanguageTxt.getText();
		MobileActionUtil.verifyContainsText(actRes, GenericLib.getProprtyValue(sFilePath, SELECT_LANGUAGE_TEXT), driver);
		
		String actRes1 = langaugeTxt.getText();
		MobileActionUtil.verifyContainsText(actRes1, GenericLib.getProprtyValue(sFilePath, LANGUAGE), driver);
	}
	
	
	/**
	 * @author Sunil S
	 * @description Method to click on done button
	 * @throws Exception
	 */
	public void clickOnDone() throws Exception{
		MobileActionUtil.clickElement(doneBtn, driver, "Done Button");
	}
	
	public void selectPlateSource(String sFilePath, String sSheet, String sTestCaseID){
		
		
		
		
		
	}
	
	
	
	

}
