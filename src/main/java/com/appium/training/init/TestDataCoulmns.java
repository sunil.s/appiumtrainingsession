package com.appium.training.init;

public interface TestDataCoulmns {

	/** VEHICLES&FINES SHEET HEADINGS **/

	String VEHICLES_SHEET_NAME = "VEHICLES";
	String TEST_CASE_ID = "TEST_CASE_ID";
	String PLATE_SOURCE = "PLATE_SOURCE";
	String PLATE_TYPE = "PLATE_TYPE";
	String PLATE_CODE = "PLATE_CODE";
	String PLATE_NUMBER = "PLATE_NUMBER";
	String SUCCESS_MSG = "SUCCESS_MSG";
	
	
	/** VALIDATIONS PROPERTY FILE KEYS **/
	
	String CONGRATULATIONS_TEXT = "CONGRATULATIONS_TEXT";
    String GOOD_EVENING_TEXT = "GOOD_EVENING_TEXT";
    String USERNAME = "USERNAME";
    String SELECT_LANGUAGE_TEXT = "SELECT_LANGUAGE_TEXT";
    String LANGUAGE = "LANGUAGE";

}
