package com.appium.training.init;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

public class GlobalVariables implements TestDataCoulmns {

	public AndroidDriver aDriver;
	public DesiredCapabilities capabilities;
	public int iPort;
	public String sUDID;
	public String sPlatformVersion;
	public String sDeviceName;
	public String sAutomationName;
	public String sXcodeOrgId;
	
}