package com.appium.training.init;



import com.appium.training.pages.SplashScreen;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

public class InitializePages {

	/** APP CLASSES **/
	public SplashScreen splashScreen = null;


	/** APP CLASSES INITIALISATION **/
	public InitializePages(AndroidDriver driver) {

		splashScreen = new SplashScreen(driver);
	
	}
}
