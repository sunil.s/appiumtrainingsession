package com.appium.training.tests;

import java.io.IOException;

import org.testng.annotations.Test;

import com.appium.training.init.InitializePages;
import com.appium.training.init.TestDataCoulmns;
import com.appium.training.library.BaseLib;
import com.appium.training.library.GenericLib;
import com.appium.training.listener.MyExtentListners;


public class TC_01 extends BaseLib implements TestDataCoulmns {
	
	@Test
	public void TC_001  () throws Exception{
		
		MyExtentListners.test.assignCategory("Top Up Parking Account using Credit or Debit Card");
		String testCaseId = "TC_001";
		
		/* Verify Splash Screen is displayed and proceed to dashboard screen */
		InitializePages initPage = new InitializePages(gv.aDriver);
		initPage.splashScreen.verifySplashScren(GenericLib.sValidationFile);
		initPage.splashScreen.clickOnDone();
		initPage.splashScreen.selectPlateSource(GenericLib.sTestData, VEHICLES_SHEET_NAME, testCaseId);
		
		
	}

}
